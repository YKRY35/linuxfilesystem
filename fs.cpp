#include "fs.h"
#include "io.h"
#include "util.h"
#include <cstdio>
#include <string>
#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;

//BlockManager


BlockManager::BlockManager(bool *mp, int num) {
	free.clear();
	for (int i = num - 1; i >= 0; --i) {
		if (!mp[i]) free.push_back(i);
	}
//	printf("size : %d\n", free.size());
}


bool BlockManager::hasBlocks(int num) {
	return free.size() >= num;
}

void BlockManager::getBlocks(int num, int *result) {
	//先调用hasBlocks
	for (int i = 0; i < num; ++i) {
		result[i] = free[free.size() - 1];
		free.pop_back();
	}
}

void BlockManager::returnBlocks(int num, int *src) {
	for (int i = 0; i < num; ++i) {
		free.push_back(src[i]);
	}
}

void BlockManager::returnOneBlock(int b) {
	free.push_back(b);
}

void BlockManager::setMap(bool *mp, int num) {
	int i;
	for (i = 0; i < num; i++) {
		mp[i] = 1;//占用
	}
	for (i = free.size() - 1; i >= 0; i--) {//free[i] 逻辑块空闲
		mp[free[i]] = 0;
	}
}

int BlockManager::remainingBlocksNum() {
	return free.size();
}

//FS

void FS::readBlock(int blockNum, char *content, int size) {
	//printf("readBlock size_per_block size %d %d\n", superBlock.SIZE_PER_BLOCK, size);
	char *src = new char[superBlock.SIZE_PER_BLOCK];
	__read(blockNum, superBlock.SIZE_PER_BLOCK, src, superBlock.SIZE_PER_BLOCK);
	//取子区间
	memcpy(content, src, size);
	delete[] src;
}

void FS::writeBlock(int blockNum, char *tar, int size) {
	//printf("size: %d\n", size);
	char *src = new char[superBlock.SIZE_PER_BLOCK];
	memcpy(src, tar, size);
	__write(blockNum, superBlock.SIZE_PER_BLOCK, src, superBlock.SIZE_PER_BLOCK);
	delete[] src;
}

void FS::format() {
	//写超级块
	SuperBlock __superBlock;
	__superBlock.is_format = true;
	
	__superBlock.free_space = (TEST_DATA_NUM - 1)*DISK_BLOCK_SIZE;
	int i;
	for (i = 0; i < TEST_INODE_NUM; i++) {
		__superBlock.inodeBitmap[i] = false;
	}
	__superBlock.inodeBitmap[0] = true;

	for (i = 0; i < TEST_DATA_NUM; i++) {
		__superBlock.dataBitmap[i] = false;
	}
	__superBlock.dataBitmap[0] = true;

	__superBlock.SIZE_PER_BLOCK = DISK_BLOCK_SIZE;
	__superBlock.B_N_INODE = TEST_INODE_NUM;
	__superBlock.B_N_DATA = TEST_DATA_NUM;
	__superBlock.INODE_DIRECT_MAX_ = INODE_DIRECT_MAX;

//	__write(0, DISK_BLOCK_SIZE, (char*) &__superBlock, sizeof(__superBlock));//写超级块
	this->superBlock = __superBlock;//拷贝到内存
	writeBlock(0, (char*)&__superBlock, sizeof(__superBlock));

	//创建根目录
	createRootDirectory(0, 0, 0);//父节点仍然是自己
}

int FS::getInodeRealAddr(int inodeBlock) {
	return 1 + inodeBlock;
}

int FS::getDataRealAddr(int dataBlock) {
	return 1 + this->superBlock.B_N_INODE + dataBlock;
}

//创建根目录
void FS::createRootDirectory(int inodeB, int dataB, int fatherInode) {//目标的inode号， 数据块号， 父目录inode
	Dir dir;
	dir.num = 1;

	DirItem dirItem;
	strcpy(dirItem.filename, FATHER_DIR_FILENAME);
	dirItem.inode = fatherInode;
	dir.items[0] = dirItem;

	//dir写入硬盘
	//__write(getDataRealAddr(dataB), superBlock.SIZE_PER_BLOCK, (char*)&dir, sizeof(dir));
	writeBlock(getDataRealAddr(dataB), (char*)&dir, sizeof(dir));

	//创建对应inode
	createRootInode(inodeB, sizeof(dir), dataB);

}

void FS::createRootInode(int inodeB, int fileSize, int dataB) {
	INODE _inode;
	_inode.createdTime = getTimeStamp();
	_inode.lastChangedTime = _inode.createdTime;
	_inode.fileSize = fileSize;
	_inode.fileType = FileType::DIRECTORY;
	_inode.directBlock[0] = dataB;
	_inode.indirectBlock = -1;

	//_inode写入硬盘
	//__write(getInodeRealAddr(inodeB), superBlock.SIZE_PER_BLOCK, (char*)&_inode, sizeof(INODE));
	writeBlock(getInodeRealAddr(inodeB), (char*)&_inode, sizeof(INODE));
}

INODE FS::readInode(int inodeB) {
	INODE _inode;
//	__read(getInodeRealAddr(inodeB), superBlock.SIZE_PER_BLOCK, (char *)&_inode, sizeof(INODE));
	readBlock(getInodeRealAddr(inodeB), (char *)&_inode, sizeof(INODE));
	return _inode;
}

void FS::getDataBsByInode(INODE inode, int **dataBs, int *blockNum){
	*blockNum = calcBlockNum(inode.fileSize);
	*dataBs = new int[*blockNum];//实际数据块地址保存在此
	if (inode.indirectBlock < 0) {//直接寻址
		memcpy(*dataBs, inode.directBlock, *blockNum * sizeof(int));
	}
	else {//间接寻址
		readBlock(getDataRealAddr(inode.indirectBlock), (char *)*dataBs, *blockNum * sizeof(int));
	}
}

void FS::readFile(INODE inode, char **tar, int *len) {
	*len = inode.fileSize;
	*tar = new char[*len];
	//需要调用者手动释放内存


	int BLOCK_SIZE = superBlock.SIZE_PER_BLOCK;
	int blockNum;
	int *dataBs;//实际数据块地址保存在此
	getDataBsByInode(inode, &dataBs, &blockNum);

	int offset = 0;
	int length = 0;
	for (int i = 0; i < blockNum; i++) {
		offset = i * BLOCK_SIZE;
		length = min(BLOCK_SIZE, *len - offset);
		//	__read(getDataRealAddr(inode.directBlock[i]), BLOCK_SIZE, *tar + offset, length);
		readBlock(getDataRealAddr(dataBs[i]), *tar + offset, length);
	}
}


//暂不做剩余空间判断，累了
//如果容量超了在文件复制过程中，会报错
void FS::copyFile(char *srcPath, char *newFilename) {
	int inodeB = getPathInodeB(srcPath);
	if (inodeB < 0) {
		return;
	}
	if (readInode(inodeB).fileType == FileType::NORMAL) {
		copyNormalFile(inodeB, newFilename);
	}
	else {
		copyDirectory(inodeB, newFilename);
	}
}

void FS::copyNormalFile(int inodeB, char *filename) {//复制普通文件
	//读取文件信息
	char *content;
	int len;
	readFile(readInode(inodeB), &content, &len);
	createFile(filename, content, len, FileType::NORMAL);
}

//注意：测试时考虑复制的文件夹是父..目录，无限递归
void FS::copyDirectory(int inodeB, char *filename) {
	Dir srcDir;
	readBlock(getDataRealAddr(readInode(inodeB).directBlock[0]), (char*)&srcDir, sizeof(srcDir));
	//printf("%d\n", srcDir.num);
	
	createDirectory(filename);
	changeDirectory(filename);

	for (int i = 1; i < srcDir.num; ++i) {//1开始
		int __inode = srcDir.items[i].inode;
		if (readInode(__inode).fileType == FileType::NORMAL) {//普通文件
			copyNormalFile(__inode, srcDir.items[i].filename);
		}
		else {//文件夹
			copyDirectory(__inode, srcDir.items[i].filename);
		}
	}

	changeDirectory("..");
}


/*
void FS::readData_struct(INODE inode, char *tar) {
	int blockSize = superBlock.SIZE_PER_BLOCK;
	if (inode.indirectBlock < 0) {//直接寻址
	//	puts("direct");
		int bn = (inode.fileSize + superBlock.SIZE_PER_BLOCK - 1) / superBlock.SIZE_PER_BLOCK;;//块数 向上取整
	//	printf("block num %d\n", bn);
		for (int i = 0; i < bn - 1; i++) {// [ 0 -> bn-2]
		//	__read(getDataRealAddr(inode.directBlock[i]), blockSize, tar + i * blockSize, blockSize);
		//	readBlock(getDataRealAddr(inode.directBlock[i]), tar + i * blockSize, blockSize)
		}
		int curOffset = (bn - 1) * blockSize;
		__read(getDataRealAddr(inode.directBlock[bn - 1]), blockSize, tar + curOffset, inode.fileSize - curOffset);
	}
	else {

	}
}*/

/*
//一个struct占用不会超过一个块
void FS::writeData_struct(INODE inode, char *src) {
	//直接寻址
	__write(getDataRealAddr(inode.directBlock[0]), superBlock.SIZE_PER_BLOCK, src, inode.fileSize);
}*/


//此处设计不知合不合理，每次创建文件都需要更新超级块，减少突然掉电造成文件系统损坏的可能
//此处可以不需要整个block写入，可以在指定位置写相应数据，后期再做优化
void FS::updateDiskSuperBlock(){
	//内存上超级块更新
	inodeManager->setMap(superBlock.inodeBitmap, superBlock.B_N_INODE);
	dataManager->setMap(superBlock.dataBitmap, superBlock.B_N_DATA);

	//物理上超级块更新
	//__write(0, superBlock.SIZE_PER_BLOCK, (char*)&superBlock, sizeof(superBlock));//写超级块
	writeBlock(0, (char*)&superBlock, sizeof(superBlock));
}

/*
核心。
一切皆文件。
文件夹，普通文件基于次函数。
*/
void FS::createFile(char *filename, char *content, int fileSize, FileType ft) {
	const int BLOCK_SIZE = superBlock.SIZE_PER_BLOCK;

	if (getCurDirItemIdx(filename) >= 0) {//文件名冲突
		puts("filename conflict");
		return;
	}

	//fileSize需要多少个块
	int blockNum = calcBlockNum(fileSize);//向上取整
	if (blockNum <= 0) {
		puts("error");
		return;
	}
	if (blockNum <= superBlock.INODE_DIRECT_MAX_) {//直接寻址
		//do nothing
	}
	else {//间接寻址
		blockNum++;
	}//如果文件太大，考虑二级间接寻址。这里不做判断

	if (!inodeManager->hasBlocks(1)) {
		puts("inode block run out");//inode 节点不够
		return;
	}
	
	if (!dataManager->hasBlocks(blockNum)) {
		puts("data block run out");//data 节点不够
		return;
	}

	int inodeBs[1];
	inodeManager->getBlocks(1, inodeBs);//分配在inodeB[0]上
	int inodeB = inodeBs[0];

	int *dataBs = new int[blockNum];
	dataManager->getBlocks(blockNum, dataBs);//dataBs 里


//	printf("%d %d\n", inodeB, dataBs[0]);

	INODE _inode;
	_inode.createdTime = getTimeStamp();
	_inode.lastChangedTime = _inode.createdTime;
	_inode.fileSize = fileSize;
	_inode.fileType = ft;
	if (blockNum <= superBlock.INODE_DIRECT_MAX_) {//直接寻址
		for (int i = 0; i < blockNum; ++i) {
			_inode.directBlock[i] = dataBs[i];
		}
		_inode.indirectBlock = -1;
	}
	else {//间接寻址
		_inode.indirectBlock = dataBs[0];//下标0 保存间接的块
	}
	//__write(getInodeRealAddr(inodeB), BLOCK_SIZE, (char*)&_inode, sizeof(_inode));//硬盘写入_inode
	writeBlock(getInodeRealAddr(inodeB), (char*)&_inode, sizeof(_inode));

/*	puts("create file blocks");
	for (int i = 0; i < blockNum; i++) {
		printf("%d ", dataBs[i]);
	}
	puts("");*/

	//写入数据块
	writeFile(content, fileSize, dataBs, blockNum);
/*	int dataBOffset = 0;
	if (blockNum <= superBlock.INODE_DIRECT_MAX_) {//直接寻址
		dataBOffset = 0;
	}
	else {
		dataBOffset = 1;//偏移一位
		//写入间接寻址块
		__write(getDataRealAddr(dataBs[0]), BLOCK_SIZE, (char*)(dataBs + 1), blockNum - 1);
	}
	//写入实际数据块（非指针）
	for (int i = dataBOffset; i < blockNum; ++i) {
		int offset = i* BLOCK_SIZE;
		int len = min(BLOCK_SIZE, fileSize - offset);
		//content 的 偏移offset 长度len 被写入
		__write(getDataRealAddr(dataBs[i]), BLOCK_SIZE, (char*)(content + offset), len);
	}*/

	//更新超级块
	updateDiskSuperBlock();

	//更新当前目录结构
	//文件大小不会改变
	strcpy(curDir.items[curDir.num].filename, filename);
	curDir.items[curDir.num].inode = inodeB;
	curDir.num++;
	//当前目录结构写入硬盘
	//writeData_struct(curInode, (char*)&curDir);
	updateDiskCurDir();
}

void FS::updateDiskCurDir() {
	writeBlock(getDataRealAddr(curInode.directBlock[0]), (char*)&curDir, sizeof(curDir));
}

//blockNum：间接寻址时包括了间接块 (+1)
void FS::writeFile(char *content, int fileSize, int *dataBs, int blockNum) {

	//写入数据块
	const int BLOCK_SIZE = superBlock.SIZE_PER_BLOCK;
	int dataBOffset = 0;
	if (blockNum <= superBlock.INODE_DIRECT_MAX_) {//直接寻址
		dataBOffset = 0;
	}
	else {
		dataBOffset = 1;//偏移一位
						//写入间接寻址块
		//__write(getDataRealAddr(dataBs[0]), BLOCK_SIZE, (char*)(dataBs + 1), blockNum - 1);
	/*	puts("test");
		printf("%d\n", getDataRealAddr(dataBs[0]));
		for (int i = 1; i <= blockNum - 1; i++) {
			printf("%d ", dataBs[i]);
		}
		puts("");*/
		writeBlock(getDataRealAddr(dataBs[0]), (char*)(dataBs + 1), (blockNum - 1) * sizeof(int));
	}
	//puts("ok");
	//写入实际数据块（非指针）
	for (int i = dataBOffset; i < blockNum; ++i) {
		int offset = (i - dataBOffset)* BLOCK_SIZE;
		int len = min(BLOCK_SIZE, fileSize - offset);
		//content 的 偏移offset 长度len 被写入
		//__write(getDataRealAddr(dataBs[i]), BLOCK_SIZE, (char*)(content + offset), len);
		writeBlock(getDataRealAddr(dataBs[i]), content + offset, len);
		
		//printf("===%d %d\n", getDataRealAddr(dataBs[i]), content[0]);
	}
}

void FS::createDirectory(char *filename) {//创建目录
	Dir dir;
	dir.num = 1;

	DirItem dirItem;
	strcpy(dirItem.filename, FATHER_DIR_FILENAME);
	dirItem.inode = curInodeB;
	dir.items[0] = dirItem;

	//创建文件（目录也是文件）

	createFile(filename, (char*)&dir, sizeof(dir), FileType::DIRECTORY);
}

int FS::getDirItemIdx(Dir dir, char *filename) {
	for (int i = 0; i < dir.num; i++) {
		if (strcmp(dir.items[i].filename, filename) == 0) {
			return i;
		}
	}
	return -1;
}

int FS::getCurDirItemIdx(char *filename) {//不存在返回-1
	return getDirItemIdx(curDir, filename);
}

int FS::getPathInodeBAndFilename(char *path, char *__filename) {
	int len = strlen(path);
	int idx = 0, lstIdx = 0;
	int inodeB = -1;

	char *_path = new char[len + 2];
	strcpy(_path, path);
	_path[len] = '\\';
	_path[len + 1] = 0;
	len += 2;

	Dir tmpDir;
	if (_path[0] == '\\') {//绝对路径
		readBlock(getDataRealAddr(0), (char *)&tmpDir, sizeof(tmpDir));
	}
	else {//相对路径
		  //从当前目录出发
		memcpy(&tmpDir, &curDir, sizeof(tmpDir));
	}


	while (idx < len) {
		if (_path[idx] == '\\') {// [lstIdx, idx)
			int fLen = idx - lstIdx;
			if (fLen > 0) {
				char *filename = new char[fLen + 1];
				memcpy(filename, _path + lstIdx, fLen);
				filename[fLen] = 0;

				strcpy(__filename, filename);

				int __idx = getDirItemIdx(tmpDir, filename);
				if (__idx < 0) {
					return -1;
				}
				inodeB = tmpDir.items[__idx].inode;

				//printf("%s %d\n",filename, inodeB);
				readBlock(getDataRealAddr(readInode(inodeB).directBlock[0]), (char*)&tmpDir, sizeof(tmpDir));
			}

			lstIdx = idx + 1;
		}
		idx++;
	}

	delete[] _path;
	return inodeB;
}

int FS::getPathInodeB(char *path){
	char filename[30];
	return getPathInodeBAndFilename(path, filename);
}

void FS::renameFile(char *src, char *rep) {
	int srcIdx = getCurDirItemIdx(src);
	if (srcIdx < 0) {
		puts("file not exist");
		return;
	}
	int idx = getCurDirItemIdx(rep);
	if (idx >= 0) {//rep已存在
		puts("filename conflict (rename)");
		return;
	}
	strcpy(curDir.items[srcIdx].filename, rep);//更新文件名
	//writeData_struct(curInode, (char*)&curDir);//写入硬盘
	//writeBlock(getDataRealAddr(curInodeB), (char*)&curDir, sizeof(curDir));
	updateDiskCurDir();
}

void FS::remove(char *filename) {
	int inodeB = getInodeBByFilename(filename);
	if (inodeB < 0) {
		return;
	}
	if (readInode(inodeB).fileType == FileType::NORMAL) {
		removeNormalFile(curInode, inodeB);
	}
	else {
		removeDirectory(curInode, inodeB);
	}

	readBlock(getDataRealAddr(curInode.directBlock[0]), (char*)&curDir, sizeof(curDir));
}

void FS::fileFillWithChar(char *filename, char x) {
	int inodeB = getInodeBByFilename(filename);
	if (inodeB < 0) {
		return;
	}

	//printf("x %d\n", x);
	INODE inode = readInode(inodeB);
	const int BLOCK_SIZE = superBlock.SIZE_PER_BLOCK;
	
	int blockNum = calcBlockNum(inode.fileSize);
	inode.lastChangedTime = getTimeStamp();

	if (inode.indirectBlock != -1) blockNum++;

	int *dataBs = new int[blockNum];
	if (inode.indirectBlock == -1) {//直接寻址
		memcpy(dataBs, inode.directBlock, blockNum * sizeof(int));
	}
	else {//间接寻址
		dataBs[0] = inode.indirectBlock;
		//readDataBlock(dataBs[0], (char*)dataBs + 1, (blockNum - 1) * sizeof(int) / sizeof(char));
		readBlock(getDataRealAddr(dataBs[0]), (char*)(dataBs + 1), (blockNum - 1) * sizeof(int));//注意dataBs+1括号
	}

	char *content = new char[inode.fileSize];
	for (int i = inode.fileSize - 1; i >= 0; --i) {
		content[i] = x;
	}
	writeFile(content, inode.fileSize, dataBs, blockNum);

	inode.lastChangedTime = getTimeStamp();
	writeBlock(getInodeRealAddr(inodeB), (char*)&inode, sizeof(inode));
}

/*
void FS::readDataBlock(int dataB, char *tar, int len) {
	__read(getDataRealAddr(dataB), superBlock.SIZE_PER_BLOCK, tar, len);
}*/

int FS::calcBlockNum(int size) {
	return (size + superBlock.SIZE_PER_BLOCK - 1) / superBlock.SIZE_PER_BLOCK;
}

int FS::getInodeBByFilename(char *filename) {
	int idx = getCurDirItemIdx(filename);
	if (idx < 0) {
		puts("file not exist");
		return -1;
	}
	return curDir.items[idx].inode;
}

//模拟系统上电初始化
void FS::init() {
	//读取超级块
	

	char *temp = new char[512];
	__read(0, 512, temp, 512);
	memcpy(&superBlock, temp, sizeof(superBlock));
	//__read(0, DISK_BLOCK_SIZE, (char*) &superBlock, sizeof(SuperBlock));//存在问题
	//readBlock(0, (char*)&superBlock, sizeof(superBlock));

	if (!this->superBlock.is_format) {//需要格式化磁盘
		puts("format disk");
		format();
	}
	puts("formated");

	//读取根目录
	curInodeB = 0;
	curInode = readInode(curInodeB);
//	cout << curInode.createdTime << " " << curInode.lastChangedTime << " " << curInode.fileSize << " " << curInode.indirectBlock << endl;
//	readData_struct(curInode, (char*) &curDir);
	readBlock(getDataRealAddr(curInode.directBlock[0]), (char*)&curDir, sizeof(curDir));

	dirStack.clear();//清空目录栈

	//读取空闲块（inode、数据块）
	inodeManager = new BlockManager(superBlock.inodeBitmap, superBlock.B_N_INODE);
	dataManager = new BlockManager(superBlock.dataBitmap, superBlock.B_N_DATA);

	//printf("%d\n", superBlock.SIZE_PER_BLOCK);
}

void FS::changeDirectory(char *path) {
	int inodeB = getPathInodeB(path);
	if (inodeB < 0) {//目录不存在
		puts("directory not exist");
		return;
	}

/*	int tmpInodeB = curDir.items[idx].inode;
	INODE tmpInode = readInode(tmpInodeB);
	if (tmpInode.fileType != FileType::DIRECTORY) {
		puts("not directory");
		return;
	}*/
	if (readInode(inodeB).fileType != FileType::DIRECTORY) {
		puts("not a directory");
		return;
	}

	
	//完成目录切换
	curInodeB = inodeB;
	curInode = readInode(inodeB);
	//readData_struct(curInode, (char*)&curDir);

	//只占一个块
	readBlock(getDataRealAddr(curInode.directBlock[0]), (char*)&curDir, sizeof(curDir));
	/*
	if (strcmp(filename, FATHER_DIR_FILENAME) == 0) {//上级目录
		if (dirStack.size() > 0) dirStack.pop_back();
	}
	else {//进入子目录
		string sFilename = filename;
		dirStack.push_back(sFilename);
	}*/
}

void FS::createNormalFile(char *filename, int size) {
	char *content = new char[size];
	memset(content, 0, size);//填充0
	createFile(filename, content, size, FileType::NORMAL);
	delete[] content;
}

void FS::getAbsolutePath(char *path) {
/*	int len = 0;
	for (int i = 0, _l = dirStack.size(); i < _l; ++i) {
		const char *fn = dirStack[i].c_str();
		strcpy(path + len, fn);
		len += strlen(fn);
		if (i < _l - 1) {
			path[len] = '\\';
			len++;
		}
	}
	path[len] = 0;*/
	int len = 0;
	char filename[30];
	getAbsolutePath_core(path, &len, filename, curInodeB, -1);
}

//当前InodeB， 子节点（上层递归）InodeB
void FS::getAbsolutePath_core(char *path, int *len, char *filename, int cInodeB, int childInodeB) {
	
	Dir tmpDir;
	INODE tmpInode = readInode(cInodeB);
	readBlock(getDataRealAddr(tmpInode.directBlock[0]), (char*)&tmpDir, sizeof(tmpDir));
	if (cInodeB == 0) {//当前是根目录
		filename[0] = '\\';
		filename[1] = 0;
		*len = 0;
	}
	else {//非根目录，向上递归
		getAbsolutePath_core(path, len, filename, tmpDir.items[0].inode, cInodeB);
	}

	//回溯设置path
	strcpy(path + *len, filename);
	*len += strlen(filename);

	//回溯时将filename设好
	for (int i = 0; i < tmpDir.num; ++i) {
		if (tmpDir.items[i].inode == childInodeB) {
			strcpy(filename, tmpDir.items[i].filename);
			int l = strlen(filename);
			filename[l++] = '\\';
			filename[l++] = 0;
			return;//回到上层
		}
	}
}

void FS::removeNormalFile(INODE dirInode, int inodeB) {
	removeNormalFile(inodeB);

	Dir dir;
	readBlock(getDataRealAddr(dirInode.directBlock[0]), (char*)&dir, sizeof(dir));
	for (int i = 0; i < dir.num; i++) {
		if (dir.items[i].inode == inodeB) {
			//目录中移除
			//i <= i + 1 , num - 2 <= num - 1
		//	printf("%d\n", i);
			for (int j = i; j <= dir.num - 2; j++) {
				strcpy(dir.items[j].filename, dir.items[j + 1].filename);
				dir.items[j].inode = dir.items[j + 1].inode;
			}
			dir.num--;
			break;
		}
	}
	writeBlock(getDataRealAddr(dirInode.directBlock[0]), (char*)&dir, sizeof(dir));
	updateDiskSuperBlock();
}

void FS::removeNormalFile(int inodeB) {
	INODE inode = readInode(inodeB);
	int blockNum;
	int *dataBs;//实际数据块地址保存在此
	getDataBsByInode(inode, &dataBs, &blockNum);
	dataManager->returnBlocks(blockNum, dataBs);//数据块删除
	inodeManager->returnOneBlock(inodeB);//inode块删除
}

void FS::removeDirectory(INODE dirInode, int inodeB) {
	//先删除文件夹内所有文件
	removeDirectoryContent(inodeB);//传入要删除文件夹的inodeB
	//再删除文件夹文件本身
	removeNormalFile(dirInode, inodeB);
}


//有较多的磁盘重读行为，这里不考虑性能。
//之后可单独建立一个缓存管理，硬盘数据根据情况缓存在内存中
void FS::removeDirectoryContent(int inodeB) {
	INODE inode = readInode(inodeB);
	Dir dir;
	readBlock(getDataRealAddr(inode.directBlock[0]), (char *)&dir, sizeof(dir));
	//除了.. ，都删
	for (int i = 1; i < dir.num; ++i) {
		int delInodeB = dir.items[i].inode;//要删除的inode号
		if (readInode(delInodeB).fileType == FileType::NORMAL) {//普通文件
			removeNormalFile(delInodeB);
		}
		else {//文件夹
			//先删除子目录所有东西
			removeDirectoryContent(delInodeB);
			removeNormalFile(delInodeB);//再删除目录
		}
	}
}


void FS::printAllFiles() {
	printAllFiles_core(curDir);
}

void FS::printAllFiles_core(Dir d) {
	int n = d.num;
	for (int i = 0; i < n; ++i) {
		P_TAB;
		printf("%s\n", d.items[i].filename);
	}
}

void FS::printFiles() {
	printFiles_core(curDir, false);
}

void FS::printFilesWithDetail() {
	printFiles_core(curDir, true);
}

void FS::printFiles_core(Dir d, bool detail) {
	int n = d.num;
	//下标0是上一级目录
	for (int i = 1; i < n; ++i) {
		P_TAB;
		if (!detail) {
			printf("%s\n", d.items[i].filename);
		}
		else {
			INODE inode = readInode(d.items[i].inode);
			char *fileType = inode.fileType == FileType::DIRECTORY ? "directory" : "normal";
			printf("%s %s %d\n", d.items[i].filename, fileType, inode.fileSize);
		}
	}
}

void FS::printBlockInfo() {
	P_TAB;
	printf("total number of inode blocks : %d\n", superBlock.B_N_INODE);
	P_TAB;
	printf("remaining number of inode blocks : %d\n", inodeManager->remainingBlocksNum());
	for (int i = 0; i < superBlock.B_N_INODE; i++) {
		if (!superBlock.inodeBitmap[i]) {
			P_TAB;
			printf("%d", i);
		}
	}
	puts("");
	P_TAB;
	printf("total number of data blocks : %d\n", superBlock.B_N_DATA);
	P_TAB;
	printf("remaining number of data blocks : %d\n", dataManager->remainingBlocksNum());
	for (int i = 0; i < superBlock.B_N_DATA; i++) {
		if (!superBlock.dataBitmap[i]) {
			P_TAB;
			printf("%d", i);
		}
	}
	puts("");
}

void FS::printFileContent(char *filename) {
	char *content;
	int len;

	int idx = getCurDirItemIdx(filename);
	if (idx < 0) {
		puts("file not exist");
		return;
	}
	int inodeB = curDir.items[idx].inode;
	INODE inode = readInode(inodeB);
	readFile(inode, &content, &len);

	const int LINE_MAX_NUM = 32;//一行最多的1字节数
	int lineNum = 0;
	P_TAB;
	for (int i = 0; i < len; ++i) {
		int data = content[i] < 0 ? content[i] + 256 : content[i];
	//	printf("%d ", data);
		printf("%X%X ", data / 16, data % 16);
		lineNum++;
		if (lineNum >= LINE_MAX_NUM) {
			puts("");
			P_TAB;
			lineNum = 0;
		}
	}
}

void FS::printFileInfo(char *filename) {
	int idx = getCurDirItemIdx(filename);
	if (idx < 0) {
		puts("file not exist");
		return;
	}
	int inodeB = curDir.items[idx].inode;
	INODE inode = readInode(inodeB);

	int blockNum = (inode.fileSize + superBlock.SIZE_PER_BLOCK - 1) / superBlock.SIZE_PER_BLOCK;

	char created_time[30], changed_time[30];
	parseTimeStamp(inode.createdTime, created_time);
	parseTimeStamp(inode.lastChangedTime, changed_time);

	P_TAB;
	printf("created time: %s\n", created_time);
	P_TAB;
	printf("last changed time %s\n", changed_time);
	P_TAB;
	printf("file size: %d\n", inode.fileSize);
	P_TAB;
	printf("file type : %s\n", inode.fileType == FileType::NORMAL ? "normal" : "directory");
	P_TAB;
	printf("addressing mode : %s addressing\n", inode.indirectBlock == -1 ? "direct" : "indirect");
	P_TAB;
	printf("inode block number logic( real ) : %d ( %d )\n", inodeB, getInodeRealAddr(inodeB));
	P_TAB;
	printf("data blocks amount : %d\n", blockNum);
	if (inode.indirectBlock == -1) {//直接寻址
		P_TAB; P_TAB;
		puts("data block number : logic ( real )");
		for (int i = 0; i < blockNum; ++i) {
			P_TAB;
			printf("%d ( %d )", inode.directBlock[i], getDataRealAddr(inode.directBlock[i]));
		}
		puts("");
	}
	else {
		int *blocks = new int[blockNum];
	//	printf("! %d %d\n", getDataRealAddr(inode.indirectBlock), superBlock.SIZE_PER_BLOCK);
	//	__read(getDataRealAddr(inode.indirectBlock), superBlock.SIZE_PER_BLOCK, (char*)blocks, blockNum * sizeof(int));
		readBlock(getDataRealAddr(inode.indirectBlock), (char *)blocks, blockNum * sizeof(int));
		for (int i = 0; i < blockNum; ++i) {
			P_TAB;
			printf("%d ( %d )", blocks[i], getDataRealAddr(blocks[i]));
		}
		puts("");
	}
}

void FS::printTree() {
	printTree_core(curDir, 2);
}

void FS::printTree_core(Dir d, int depth) {
	for (int i = 0; i < d.num; ++i) {
		if (strcmp(d.items[i].filename, FATHER_DIR_FILENAME) == 0) {
			continue;
		}
		for (int j = 0; j < depth; ++j) P_TAB;
		printf("%s\n", d.items[i].filename);

		INODE __inode = readInode(d.items[i].inode);
		if (__inode.fileType == FileType::DIRECTORY) {//文件夹，向下递归
			Dir sub;
			readBlock(getDataRealAddr(__inode.directBlock[0]), (char *)&sub, sizeof(sub));
			printTree_core(sub, depth + 1);
		}
	}
}

void FS::exit() {
	updateDiskSuperBlock();
}