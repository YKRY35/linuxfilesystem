#pragma once

#include<iostream>
#include<vector>
using namespace std;

#define INODE_BITMAP_SIZE 105 //inode 位图大小 （略加大）
#define DATA_BITMAP_SIZE 205 //block 位图大小 （略加大）

#define TEST_INODE_NUM 99
#define TEST_DATA_NUM 200

#define INODE_DIRECT_MAX 10 //一个inode节点直接寻址指针数量最大值

#define DIR_ITEM_FILENAME_MAX_LENGTH 13 //文件名最大长度+1  (\0)
#define DIR_ITEMS_MAX_NUM 25 //文件目录

#define FATHER_DIR_FILENAME ".." //父节点文件名

/*
block 0 : super block
block [1 -> BN_N_IN + 1) : inode
block [BN_N_IN + 1 -> END) : data
*/
//超级块
struct SuperBlock {
	bool is_format;//是否格式化过
	int free_space;//剩余空间大小 字节
	bool inodeBitmap[INODE_BITMAP_SIZE];//inode 位图
	bool dataBitmap[DATA_BITMAP_SIZE];//data 位图

	int INODE_DIRECT_MAX_;//inode直接寻址最大数量
	int SIZE_PER_BLOCK;//每个块的大小 字节
	int B_N_INODE;//INODE占用的块数 block number of inode
	int B_N_DATA;//数据占用的块数
};

enum FileType {
	DIRECTORY, NORMAL
};

//文件描述节点
struct INODE {
	long long createdTime; //创建时间 time stamp
	long long lastChangedTime;//最后更改的时间 time stamp
	int fileSize;//文件大小
	FileType fileType;//文件类型
	int directBlock[INODE_DIRECT_MAX];//直接寻址
	int indirectBlock;//间接寻址
};

//文件夹条目
struct DirItem {
	char filename[DIR_ITEM_FILENAME_MAX_LENGTH]; //文件名
	int inode; //对应inode地址
};

//文件夹结构
struct Dir {
	int num;//条目数量
	DirItem items[DIR_ITEMS_MAX_NUM];//所有条目
};



//空闲块管理器
//单独分离便于调试，便于更改空闲块搜寻算法（先使用简单的vector，后期再考虑改为成组链接法）
class BlockManager {
public:
	BlockManager(bool *mp, int num);
	bool hasBlocks(int num);//判断是否有空闲块
	void getBlocks(int num, int *result);//拿空闲块
	void returnBlocks(int num, int *src);//归还空闲块
	void returnOneBlock(int b);//归还一个空闲块
	void setMap(bool *mp, int num);//更新空闲map信息
	int remainingBlocksNum();//剩余块数
private:
	vector<int> free;

};

//涉及到的inode，data地址均为逻辑地址，通过getInodeRealAddr，getDataRealAddr转化为物理地址
class FS {
public:
	void format();//格式化
	void init();
	void createDirectory(char *s);//创建目录
	void createNormalFile(char *filename, int size);//创建普通文件
	void fileFillWithChar(char *filename, char x);//文件填充指定字符
	void remove(char *filename);

	void changeDirectory(char *path);//进入某个目录
	void renameFile(char *src, char *rep);//重命名
	void getAbsolutePath(char *path);//获取绝对路径
	void printFileInfo(char *filename);//显示文件信息
	void printFileContent(char *filename);//显示文件内容

	void copyFile(char *srcPath, char *newFilename);

	void printAllFiles();//打印所有文件
	void printFiles();//打印文件
	void printFilesWithDetail();//
	void printTree();//打印树形目录
	void printBlockInfo();

	void exit();//退出系统
private:
	SuperBlock superBlock;//读入内存的超级块

	Dir curDir;//当前目录
	INODE curInode;
	int curInodeB;//当前目录inode号

	vector<string> dirStack;//目录栈

	BlockManager *inodeManager;//inode管理器
	BlockManager *dataManager;//数据块管理器

	void readBlock(int blockNum, char *content, int size);//所有的磁盘读经过这个函数：写入
	void writeBlock(int blockNum, char *tar, int size);//所有的磁盘写经过这个函数
	
	void createRootDirectory(int inodeB, int dataB, int fatherInode);//创建根目录
	void createRootInode(int inodeB, int fileSize, int directBlock);//创建根inode

	INODE readInode(int inodeB);//从硬盘读取inode
	//void readData_struct(INODE inode, char *tar);//从data区读取结构体
	//void writeData_struct(INODE inode, char *src);//向data区写结构体
	//void readDataBlock(int dataB, char *tar, int len);//读取数据块信息
	void updateDiskSuperBlock();//更新硬盘中的超级块
	void updateDiskCurDir();//更新硬盘中的CurDir
	int getInodeBByFilename(char *filename);//文件名获取inode号
	int calcBlockNum(int size);//计算需要的块数

	void createFile(char *filename, char *content, int fileSize, FileType ft);//申请创建文件
	void writeFile(char *content, int fileSize, int *dataBs, int blockNum);//向硬盘写入文件
	void readFile(INODE inode, char **tar, int *len);//根据inode从硬盘读取数据
	void copyNormalFile(int inodeB, char *filename);
	void copyDirectory(int inodeB, char *filename);

	int getInodeRealAddr(int inodeBlock);//获取inode节点真实地址
	int getDataRealAddr(int dataBlock);//获取数据块真实地址

	int getDirItemIdx(Dir dir, char *filename);
	int getCurDirItemIdx(char *filename);//找到当前目录文件名对应的dir的item下标
	int getPathInodeB(char *path);//获取路径对应目录或文件inode
	int getPathInodeBAndFilename(char *path, char *filename);
	void getDataBsByInode(INODE inode, int **dataBs, int *blockNum);

	void removeNormalFile(INODE dirInode, int inodeB);//删除文件  （所属目录节点， 待删文件inode号）
	void removeNormalFile(int inodeB);//删除文件
	void removeDirectory(INODE dirInode, int inodeB);//删除文件夹
	void removeDirectoryContent(int inodeB);//删除这个目录下所有文件
	void printAllFiles_core(Dir d);//打印所有文件
	void printFiles_core(Dir d, bool detail);//打印文件
	void printTree_core(Dir d,int depth);//打印树
	void getAbsolutePath_core(char *path, int *len, char *filename, int cInodeB, int childInodeB);
};