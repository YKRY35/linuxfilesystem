#pragma once

#include <cstdio>

#define DISK_BLOCK_SIZE 512 //一个块大小 字节
#define DISK_SIZE 300*512 //磁盘大小 字节

#define DISK_FILENAME "__disk"

#define TAB_N 4
#define P_TAB for(int __i__=0; __i__<TAB_N;++__i__) printf(" ");

//生成整个模拟磁盘文件
void createDisk();

//读磁盘
void __read(int blockNum, int blockSize, char *tar, int len);

//写
void __write(int blockNum, int blockSize, char *src, int len);