#include "io.h"
#include "fs.h"

#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

#define CREATE_DISK_FLAG false

#define CMDS_NUM 16

bool sEqual(const char *a, const char *b) {
	return strcmp(a, b) == 0;
}

char cmdsForDetail[CMDS_NUM][50] = {
	"mkdir ?", "cd ?", "ls ?", "ll ?", "tree ?", "ren ?",
	"touch ?", "file ?", "cat ?", "fill ?",
	"rm ?", "cp ?", "binfo ?", "help ?", "cls ?", "exit ?"
};

char cmds[CMDS_NUM][50] = {
	"mkdir (filename)", "cd (path)", "ls", "ll", "tree", "ren (src filename) (new filename)",
	"touch (filename) (filesize)", "file (filename)", "cat (filename)", "fill (filename) (fill word)",
	"rm (filename)", "cp (path) (new filename)", "binfo", "help", "cls", "exit"
};

char cmdsWithDetail[CMDS_NUM][200] = {
	"mkdir (filename): make a directory",
	"cd (path): change directory to given path",
	"ls: print all visible files and directories",
	"ll: print all files and directories",
	"tree: print the tree struct of current directory",
	"ren (src filename) (new filename): rename file or directory",
	"touch (filename) (filesize): create one file. default filesize is 100. file fill with 0",
	"file (filename): print file info",
	"cat (filename): print file content",
	"fill (filename) (fill word): fill given file with (fill word)",
	"rm (filename): remove file or directory",
	"cp (path) (new filename): copy file from (path) to current directory). new file's name is (new filename)",
	"binfo: print block info",
	"help: display help documentation for all commands",
	"cls: clear the screen",
	"exit: exit system"
};

void printCmds() {
	for (int i = 0; i < CMDS_NUM; ++i) {
		P_TAB;
		printf("%s\n", cmds[i]);
	}
}

void printUsage() {
	for (int i = 0; i < CMDS_NUM; ++i) {
		P_TAB;
		printf("%s\n", cmdsWithDetail[i]);
	}
}

void printUsage(int n) {
	P_TAB;
	printf("%s\n", cmdsWithDetail[n]);
}

bool checkForDetail(char *cmds) {
	for (int i = 0; i < CMDS_NUM; ++i) {
		if (strcmp(cmdsForDetail[i], cmds) == 0) {
			printUsage(i);
			return true;
		}
	}
	return false;
}

int main() {
	
	if (CREATE_DISK_FLAG) {
		createDisk();
		return 0;
	}

	FS fs;

	fs.init();

	//system("pause");
	system("cls");

	printCmds();

	char path[300];
	char cmd[100];
	char type[20];
	char filename[20];
	char srcPath[100];
	char newFilename[20];
	int fillNum = 0;
	char tarFilename[20];
	char params[20];
	int fileSize;
	while (true) {
		fs.getAbsolutePath(path);
		puts("");
		printf("%s>", path);

		cin.getline(cmd, 100);
		
		if (checkForDetail(cmd)) {
			continue;
		}

		type[0] = 0;
		sscanf(cmd, "%s", type);

		if (sEqual(type, "mkdir")) {//创建文件夹
			sscanf(cmd, "%s %s", type, filename);
			fs.createDirectory(filename);
		}
		else if (sEqual(type, "cd")) {//切换文件夹
			filename[0] = 0;
			sscanf(cmd, "%s %s", type, filename);
			if (strlen(filename) > 0) {
			//	printf("%s\n", filename);
				fs.changeDirectory(filename);
			}
		}
		else if (sEqual(type, "ls")) {//显示文件
		//	fs.printAllFiles();
			params[0] = 0;
			sscanf(cmd, "%s %s", type, params);
			if (strlen(params) > 0) {
				fs.printFilesWithDetail();
			}
			else {
				fs.printFiles();
			}
		}
		else if (sEqual(type, "ll")) {//显示所有文件
			fs.printAllFiles();
		}
		else if (sEqual(type, "tree")) {
			fs.printTree();
		}
		else if (sEqual(type, "ren")) {
			filename[0] = 0;
			tarFilename[0] = 0;
			sscanf(cmd, "%s %s %s", type, filename, tarFilename);
			if (strlen(filename) > 0 && strlen(tarFilename) > 0) {
			//	printf("%s %s\n", filename, tarFilename);
				fs.renameFile(filename, tarFilename);
			}
		}
		else if (sEqual(type, "touch")) {
			filename[0] = 0;
			fileSize = 100;//默认100字节
			sscanf(cmd, "%s %s %d", type, filename, &fileSize);
		//	printf("%s %d\n", filename, fileSize);
			if (strlen(filename) > 0) {
				fs.createNormalFile(filename, fileSize);
			}
		}
		else if (sEqual(type, "cls")) {
			system("cls");
		}
		else if (sEqual(type, "binfo")) {
			fs.printBlockInfo();
		}
		else if (sEqual(type, "cat")) {//打印文件内容（十六进制显示)
			filename[0] = 0;
			sscanf(cmd, "%s %s", type, filename);
			if (strlen(filename) > 0) {
				fs.printFileContent(filename);
			}
		}
		else if (sEqual(type, "fill")) {
			filename[0] = 0;
			fillNum = -10000;
			sscanf(cmd, "%s %s %d", type, filename, &fillNum);
			if (strlen(filename) > 0 && fillNum != -10000) {
				fs.fileFillWithChar(filename, fillNum);
			}
		}
		else if (sEqual(type, "rm")) {
			filename[0] = 0;
			sscanf(cmd, "%s %s", type, filename);
			if (strlen(filename) > 0) {
				fs.remove(filename);
			}
		}
		else if (sEqual(type, "cp")) {
			srcPath[0] = 0;
			newFilename[0] = 0;
			sscanf(cmd, "%s %s %s", type, srcPath, newFilename);
			if (strlen(srcPath) > 0 && strlen(newFilename) > 0) {
				fs.copyFile(srcPath, newFilename);
			}
		}
		else if (sEqual(type, "file")) {//打印文件内容（十六进制显示)
			filename[0] = 0;
			sscanf(cmd, "%s %s", type, filename);
			if (strlen(filename) > 0) {
				fs.printFileInfo(filename);
			}
		}
		else if (sEqual(type, "help")) {
			printUsage();
		}
		else if (sEqual(type, "exit")) {
			fs.exit();
			break;
		}
	}

	system("pause");
	return 0;
}