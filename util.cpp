#include "util.h"
#include <cstdio>

long long getTimeStamp() {
	return time(NULL);
}

void parseTimeStamp(time_t tt, char *time) {
	tm* t = localtime(&tt);
	sprintf(time, "%d-%02d-%02d %02d:%02d:%02d",
		t->tm_year + 1900,
		t->tm_mon + 1,
		t->tm_mday,
		t->tm_hour,
		t->tm_min,
		t->tm_sec);
}