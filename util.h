#pragma once

#include <time.h>


long long getTimeStamp();

void parseTimeStamp(time_t tt, char *time);